import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  titulo = "Otonix Motors"

  cards = [
    {
     titulo: "Kawasaki Z900",
      subtitulo: "Kawasaki",
      conteudo: "O espírito Z encontra sua expressão mais recente no Z900 com estilo Sugomi. Potência ao máximo, manuseio instintivo e controle de tração redefinem a experiência supernaked. Luzes LED e um display TFT trazem a mais recente tecnologia. Ultrapasse os limites e domine as ruas.",
      foto: "https://www.motonline.com.br/noticia/wp-content/uploads/2021/12/kawasaki-z-900-r-edition.jpg"
    },
    {
      titulo: "MT-09",
      subtitulo: "Yamaha",
      conteudo: "Além do visual que a afasta da MT-07, com novo farol duplo em LEDs e grandes mudanças na traseira, mantém o motor de 3 cilindros e 847 cm³ com 115 cv e 8,9 kgfm de torque ligado ao câmbio de seis marchas.",
      foto: "https://cdn.motor1.com/images/mgl/NvbmX/s1/4x3/yamaha-mt-09-2020.webp"
    },
    {
      titulo: "Fazer 250",
      subtitulo: "Yamaha",
      conteudo: "Num mercado de motos cada vez mais dinâmico, a Fazer 250 é um produto bastante popular e um degrau natural para quem vem da categoria das 150/160 cilindradas, como no caso da Yamaha Factor 150, por exemplo.",
      foto: "https://http2.mlstatic.com/D_NQ_NP_645540-MLB47888975801_102021-O.jpg"
    },
    {
      titulo: "Moto Elétrica infantil Princesas",
      subtitulo: "Bandeirante",
      conteudo: "Sabe aquele presente para o seu filho ou sua filha que vai deixar ele(a) pulando de felicidade? Este presente é a Moto Eletrica Infantil Bandeirante XT3 6V! Esta incrível mini moto elétrica infantil vai fazer a alegria da sua criança!",
      foto: "https://a-static.mlcdn.com.br/1500x1500/moto-eletrica-infantil-princesas-disney-2-marchas-6v-bandeirante/magazineluiza/221393000/2ca95cd33a7b09d051c19464431903cc.jpg"
    }
  ]
  constructor() {}

}


